package m02;

public class Demo1Variables {
  public static void main(String[] args) {
    final int COUNT_TO = 20;
    // casting a byte
    int count=(byte)77;
    String msg = "start count is " ;
    // using String functions
    System.out.println(msg.concat(count+"").toUpperCase());
    Integer dou_bled=0;

    Boolean test = true;
    // a unicode literal
    char letter='\u00F1';
    while (test){
      count++;
       dou_bled = count * 2;
      test = count < COUNT_TO;
      System.out.println(letter);
      letter++;
    }
    System.out.println("test is " + test);
    System.out.print("count is " + count+"\n");
    System.out.println("twice count is " + dou_bled);
    System.out.print("W\tto\tJava\n");
    System.out.println("Welcome to 'Java'");

  }
}
