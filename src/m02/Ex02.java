package m02;

public class Ex02 {
  public static void main(String[] args) {
    int firstNumber = 2000000000;
    int secondNumber = 2000000000;
    long longNumber1 = 10000, longNumber2 = 10000;
    // casting this way works and gives the correct result
    // long sum=(long)(firstNumber+ secondNumber);
    // casting this way works and gives the correct result
    long sum = (long) firstNumber + (long) secondNumber;
    // casting this way works and gives a wrong result because the number does not fit into an int
   // int sum = (long) (firstNumber +  secondNumber);

    // casting this way works and gives a wrong result because the number does not fit into an int
    int result = (int) (longNumber1 + longNumber2); // rather this casting for int words

    System.out.println("The sum of the two numbers is : " + sum);
    System.out.println("The sum of long numbers is : " + result);
    // The sum of the two numbers is : -294967296 get this result declaring them as intergers
  }
}
