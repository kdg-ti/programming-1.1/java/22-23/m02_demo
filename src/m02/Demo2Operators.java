package m02;

public class Demo2Operators {
  public static void main(String[] args) {
    int length = 4;
    int width = 3;
    System.out.println("perimeter: " + 2*length + 2*width);
    // If we want to optimise, by multiplying by two only once
    // we need brackets to override the default operator precedence
    System.out.println("perimeter: " + (2*(length + width)) );
    // same with postfix increment
    System.out.println("perimeter: " + (2*(length++ + width)) );
    // incrementing takes effect after use
    System.out.println("length is now " + length);

    // logical
    int age=85;
    System.out.print("Age "+ age + " is part of the ");
    // single | always tests both logical expressions
    // shortcut || only tests second expression if it can impact the result
    // (if the first expression is true, the value of the second expression does not matter
    if (age < 18 | age > 67) {
      System.out.print("in");
    }
    System.out.println("active population");

    // demonstrating the XOR operator
    if (age < 18 ^ age > 67) {
      System.out.print("in");
    }
    int a=10;
    System.out.println(a++);
    System.out.println(a);
    a=10;
    int b=3;
    System.out.println(b-- - ++a);
    System.out.println(b);




  }
}
